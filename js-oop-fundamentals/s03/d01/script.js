//In JS, classes can be created by using the "class" keyword and {}.
//Naming convention for classes: begin with uppercase letters

/*
	Syntax:
		class <Name> {
	
		}
*/

//Student class
class Student {
	//Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

	//to enable students instantiated from this class to have distinct names and emails
	//our constructor must be able to accept name and email arguments
	//which it will then use to set the value of the object's corresponding properties
	constructor(name, email, grades) {
	    this.name = name;
	    this.email = email;
	    this.grades = this.validateGrades(grades) ? grades : undefined;
	  }

	  validateGrades(grades) {
	    if (
	      Array.isArray(grades) &&
	      grades.length === 4 &&
	      grades.every((grade) => typeof grade === "number" && grade >= 0 && grade <= 100)
	    ) {
	      return true;
	    }
	    return false;
	  }

/*
	The login() method logs a message to the console indicating that the student with the specific email has logged in.

	The logout() method logsa message to the console indicating that the student with the specific email has logged out.

	The listGrades() method logs a message to the console displaying the name of the student and their quarterly grade averages.

	The computeAve() method calculates the average of the student's grades by iterating over the this.grades array and summing up all the grades.

	The result is stored in the this.gradeAve property.

	The willPass() method determines if the student passed or failed based on their average grade. It calls the computeAve() method to calculate the average grade and then checks if the gradeAve property is greater than or equal to 85. The result is stored in the this.passed property.

	The willPassWithHonors() method determines if the student passed with honors based on their average grade. It first checks if the student passed (this.passed is true) and then checks if the gradeAve property is greater than or equal to 90. The result is stored in the this.passedWithHonors property.
*/	
	//class methods
		//these are common to all instances
	
	login = () => {
		console.log(`${this.email} has logged in`);

		return this;
	}

	logout = () => {
		console.log(`${this.email} has logged out`);

		return this;
	}

	listGrades = () => {
	    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);

	    return this;
	  }

	computeAve = () => {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum / 4;

		console.log(`${this.name}'s average grade is ${this.gradeAve}`);
		return this;
	}

	willPass = () => {
		this.passed = this.computeAve() >= 85;

		if(this.passed) {
			console.log(`${this.name} passed`)
		} else {
			console.log(`${this.name} failed`)
		}

		return this;
	}

	willPassWithHonors = () => {
		if (this.passed) {
			if (this.passWithHonors = this.gradeAve >= 90) {
				console.log(`Passed with honors`)
			} else {

			}
		} else {
			this.passWithHonors = false;
			console.log(`No honors`);
		}
		return this;
	}

}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
