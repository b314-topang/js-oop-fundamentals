//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

// Student One
let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	// Functions
	login() {
		return `${this.email} has logged in`
	},

	logout() {
		return `${this.email} has logged out`
	},

	listGrades() {
		return `${this.name}'s quarterly grade averages are ${this.grades}`
	},

	getQuarterly() {
		const sum = this.grades.reduce((total, grade) => total + grade, 0);
		const avg = sum/this.grades.length;

		return avg
	},

	willPass() {
		const average = this.getQuarterly();

		return average >= 85;
	},

	willPassWithHonors() {
		const average = this.getQuarterly();

		return this.willPass() && average >= 90
	}
}

// Student Two
let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	// Functions
	login() {
		return `${this.email} has logged in`
	},

	logout() {
		return `${this.email} has logged out`
	},

	listGrades() {
		return `${this.name}'s quarterly grade averages are ${this.grades}`
	},

	getQuarterly() {
		const sum = this.grades.reduce((total, grade) => total + grade, 0);
		const avg = sum/this.grades.length;

		return avg
	},

	willPass() {
		const average = this.getQuarterly();

		return average >= 85;
	},

	willPassWithHonors() {
		const average = this.getQuarterly();

		return this.willPass() && average >= 90;
	}
}

// Student Three
let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	// Functions
	login() {
		return `${this.email} has logged in`
	},

	logout() {
		return `${this.email} has logged out`
	},

	listGrades() {
		return `${this.name}'s quarterly grade averages are ${this.grades}`
	},

	getQuarterly() {
		const sum = this.grades.reduce((total, grade) => total + grade, 0);
		const avg = sum/this.grades.length;

		return avg
	},

	willPass() {
		const average = this.getQuarterly();

		return average >= 85;
	},

	willPassWithHonors() {
		const average = this.getQuarterly();

		return this.willPass() && average >= 90;
	}
}

// Student Four
let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	// Functions
	login() {
		return `${this.email} has logged in`
	},

	logout() {
		return `${this.email} has logged out`
	},

	listGrades() {
		return `${this.name}'s quarterly grade averages are ${this.grades}`
	},

	getQuarterly() {
		const sum = this.grades.reduce((total, grade) => total + grade, 0);
		const avg = sum/this.grades.length;

		return avg
	},

	willPass() {
		const average = this.getQuarterly();

		return average >= 85;
	},

	willPassWithHonors() {
		const average = this.getQuarterly();

		return this.willPass() && average >= 90;
	}
}

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents() {
		let count = 0;

		for (let student of this.students) {
			if (student.willPassWithHonors()) {
				count++;
			}
		}

		return count;
	},

	honorsPercentage() {
		const percentage = (this.countHonorStudents()/this.students.length) * 100;

		return percentage;
	},

	retrieveHonorStudentsInfo() {
		let honorStudentsInfo = [];
		for (let student of this.students) {
			if (student.willPassWithHonors()) {
				honorStudentsInfo.push({
					email: student.email,
					averageGrade: student.getQuarterly()
				});
			}
		}

		return honorStudentsInfo;
	},

	sortHonorStudentsByGradeDesc() {
		const sortedStudents = this.retrieveHonorStudentsInfo().sort((a, b) => b.averageGrade - a.averageGrade);

		return sortedStudents;
	}

}



