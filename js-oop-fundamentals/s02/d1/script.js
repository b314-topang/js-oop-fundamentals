//use an object literal: {} to create an object representing a user
	//encapsulation
	//whenever we add properties or methods to an object, we are performing ENCAPSULATION
	//the organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
	//(the scope of encapsulation is denoted by object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	//methods
		//add the funcationalities available to a student as object methods
		//the keyword "this" refers to the object encapsulating the method where "this" is called

	login() {
		console.log(`${this.email} has logged in`)
	},

	logout() {
		console.log(`${this.email} has logged out`)
	},

	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},

	//Mini-Activity
		//Create a function that will get the quarterly average of studentOne's grade
	getQuarterly() {
		const sum = this.grades.reduce((total, grade) => total + grade, 0);
		const avg = sum/this.grades.length;

		return avg
	},

	//Mini-Activity 2
		//Create a function that will return true if average grade is >=85, false otherwise
	hasPassed() {
		const average = this.getQuarterly();
		return average >= 85;
	},

	//Mini-Activity 3
		//Create a function called willPassWithHonors() that returns true if the student has passed and their average is >=90. The function returns false if either one is not met.
	willPassWithHonors() {
		const average = this.getQuarterly();
		return this.hasPassed() && average >= 90;
	}

}
	
//log the content of studentOne's encapsulated information in the console
console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quaterly grade averages area ${studentOne.grades}`);

console.log(studentOne.getQuarterly()); // Output: 84.75
console.log(studentOne.hasPassed()); // Output: false
console.log(studentOne.willPassWithHonors()); // Output: false